package com.zuitt.example;

import java.util.Scanner;

public class Exercise {
    public static void main(String[] args) {
        Scanner numberScanner = new Scanner(System.in);

        System.out.println("Enter a Year: " );
        int leapYear = numberScanner.nextInt();

        if(leapYear % 4 == 0){
            System.out.println(leapYear + " is a leap year");
        } else {
            System.out.println(leapYear + " is not a leap year");
        }

    }
}
