package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Array {
    public static void main(String[] args) {

        //[SECTION] Java Collection

        /*
        * are single unit of objects
        * data with relevant and connected values
        * */

        //[SECTION] Arrays
        /*
        * In Java, arrays are container of values of the same type give a predefined amount of values.
        * Java arrays are more rigid, once the size and data type are defined, they can no longer be changed. */

        /*SYNTAX: Array declaration
        * dataType[] identifier = new dataType[numOfElements];*/

        int[] intArray = new int[5];

        intArray[0] = 200;
        intArray[1] = 3;
        intArray[2] = 25;
        intArray[3] = 99;
        intArray[4] = 50;

        //Will result error -> out of bounds
        //intArray[5] = 21;

        //This will return memory address
        System.out.println(intArray);
        System.out.println(Arrays.toString(intArray));

        // String Array
        //Syntax: Array declaration with initialization
        //dataType[] identifier = {elementA, elementB, elementC,...}

        String[] names = {"John", "Jane", "Joe"};

        System.out.println(Arrays.toString(names));

        //Sample Java Array Method

        //Sort
        Arrays.sort(intArray);
        System.out.println("Order of items after sort() method " + Arrays.toString(intArray));

        String[][] classroom = new String[3][3];

        //first row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";

        //second row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "Junjun";
        classroom[1][2] = "Jobert";

        //third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        System.out.println(Arrays.deepToString(classroom));

        //[SECTION] ArrayList
        //are resizeable arrays, wherein elements can be added or removed
        //Syntax: ArrayList
        //ArrayList<T> identifier = new ArrayList<T>();

        //Declaring ArrayList
        ArrayList<String> students = new ArrayList<String>(Arrays.asList("Jane", "Mike"));

        //Add elements
        students.add("John");
        students.add("Paul");
        students.add("Jd");

        System.out.println(students);

        //Getting an element
        System.out.println(students.get(4));

        //Adding an element in a specific index
        students.add(1,"Joe");
        System.out.println(students);

        //update element in a specific index
        students.set(0, "George");
        System.out.println(students);

        //remove element in a specific index
        students.remove(1);
        System.out.println(students);
        System.out.println(students.size());

        //remove all element
        students.clear();
        System.out.println(students);

        //get arrayList size
        System.out.println(students.size());


        //[SECTION] Hashmap
        //Syntax: HashMap
        //HashMap<T, T> identifier = new HashMap<T, T>();

        HashMap<String, Integer> Job_position = new HashMap<String, Integer>();

        //Adding element to a HashMap
        Job_position.put("Brandon", 5);
        Job_position.put("Alice", 3);
        System.out.println(Job_position);

        System.out.println(Job_position.get("Brandon"));
        System.out.println(Job_position.values());
        Job_position.clear();
        System.out.println(Job_position);
    }

}
