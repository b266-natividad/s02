package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class WDC043_S2_A2 {
    public static void main(String[] args) {
        int[] PrimeNumbers = new int[5];

        PrimeNumbers[0] = 2;
        PrimeNumbers[1] = 3;
        PrimeNumbers[2] = 5;
        PrimeNumbers[3] = 7;
        PrimeNumbers[4] = 11;


        System.out.println("The first prime number is: " + PrimeNumbers[0]);
        ArrayList<String> friends = new ArrayList<String>(Arrays.asList("John", "Joe", "Chloe", "Zoey"));
        System.out.println("My friends are " + friends);

        HashMap<String, Integer> inventory = new HashMap<String, Integer>();


        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);
        System.out.println("Our current inventory consists of " + inventory);


    }
}